/* 
 * Cave.java - Contains all the properties required to create a cave. 
 * Author: Dimitar Hristov, 40201757
 * Module: Artificial Intelligence (SET08118)
 */
public class Cave 
{
	// Values for every single cave
	private static int numberOfCaves = 1;
	private int caveID;
	private double xCoord;
	private double yCoord;

	// Values used in the A* searching method
	// The G score is the cost from the start to the current node
	// The H score is the euclidean distance to the goal - heuristic value
	// The F score is the addition of the G and the H score
	private double g_score;
	private double h_score;
	private double f_score;

	Cave(double x, double y)
	{
		this.xCoord = x;
		this.yCoord = y;
		this.caveID = numberOfCaves;
		numberOfCaves += 1;
	}
	public double getX()
	{
		return this.xCoord;
	}
	public double getY()
	{
		return this.yCoord;
	}
	public int getID()
	{
		return this.caveID;
	}
	public double getGscore()
	{
		return this.g_score;
	}
	public double getHscore()
	{
		return this.h_score;
	}
	public double getFscore()
	{
		return this.f_score;
	}
	public void setGscore(double gScore)
	{
		this.g_score = gScore;
	}
	public void setHscore(double hScore)
	{
		this.h_score = hScore;
	}
	public void setFscore(double fScore)
	{
		this.f_score = fScore;
	}

	// Overriding the print method for caves.
	public String toString()
	{
		return "Cave number "+this.caveID+" is at X= "+this.xCoord+" and Y= "+this.yCoord;
	}
}
