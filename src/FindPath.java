/* 
 * FindPath.java - This class contains the A* searching method
 * Author: Dimitar Hristov, 40201757
 * Module: Artificial Intelligence (SET08118)
 */
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeMap;


public class FindPath {
	private Cave rootCave;
	private Cave goalCave;
	private HashMap<Integer,Cave> caveMap;
	private boolean[][] connected;
	private Graph visualisation;
	private double overallDistance = 0;


	FindPath(Cave rootCave, Cave goalCave, 	HashMap<Integer,Cave> caveMap, boolean[][] connected, Graph visualisation)
	{
		this.rootCave = rootCave;
		this.goalCave = goalCave;
		this.caveMap = caveMap;
		this.connected = connected;
		this.visualisation = visualisation;
	}

	// Calculates the euclidean distance between two points
	private double distance(Cave caveOne, Cave caveTwo)
	{
		double distance = Math.sqrt(Math.pow((caveTwo.getX()-caveOne.getX()), 2) + Math.pow((caveTwo.getY()-caveOne.getY()), 2));
		return distance;
	}	

	// Returns a list of Caves that are connected with the 'tempCave'
	private ArrayList<Cave> neighbour(Cave tempCave)
	{
		int from = tempCave.getID() - 1;
		ArrayList <Cave> neightbours = new ArrayList<Cave>();

		// Goes through the connected matrix
		for (int index = 0; index < this.connected.length; index++)
		{
			if (this.connected[from][index]==true)
			{
				neightbours.add(this.caveMap.get(index+1));
			}
		}
		return neightbours;
	}

	// Checks the open list for the smallest score
	private Cave lowestScore(TreeMap<Integer, Cave> openList)
	{
		// Set the minimum value to be the largest double value 
		double minCurrentValue = Double.MAX_VALUE;
		Cave nextCave = null;

		// For every neighbour check which one is closest to the Goal
		for(Cave c : openList.values())
		{
			double score = c.getFscore();
			if (minCurrentValue > score)
			{
				minCurrentValue = score;
				nextCave = c;
			}
		}
		return nextCave;
	}

	// A String representation of the OpenList
	private String openListString(Set<Integer> set)
	{
		String output = "";

		for (Integer c : set)
		{
			output += c + "; ";
		}
		return output;
	}

	// A String representation of all the neighbours
	private String neighboursString(Cave tempCave)
	{
		String output = "";

		int from = tempCave.getID() - 1;

		// Goes through the connected matrix
		for (int index = 0; index < this.connected.length; index++)
		{
			if (this.connected[from][index]==true)
			{
				output += this.caveMap.get(index+1).getID()+"; ";
			}
		}
		return output;
	}
	
	// String representation of the CurrentCave
	private String currentCaveInfo(Cave currentCave)
	{
		double hScore = Math.round(currentCave.getHscore()*100)/100d;
		double gScore = Math.round(currentCave.getGscore()*100)/100d;
		double fScore = Math.round(currentCave.getFscore()*100)/100d;
		
		return "* Current cave: "+currentCave+" and the scores are:\n\t   h_score: "+hScore+", g_score: "+gScore +", f_score: "+fScore+"\n";
	}

	// The A* search method
	public void aStarSearch()
	{
		TreeMap<Integer, Cave> openList = new TreeMap<Integer,Cave>();
		TreeMap<Integer, Cave> closedList = new TreeMap<Integer, Cave>();
		TreeMap<Integer, Cave> parentPath = new TreeMap<Integer,Cave>();
		boolean goalReached = false;

		// Set the scores for the starting cave
		this.rootCave.setHscore(this.distance(this.rootCave, this.goalCave));
		this.rootCave.setGscore(0);
		this.rootCave.setFscore(this.rootCave.getGscore() + this.rootCave.getHscore());
		// Add the starting cave to the openList
		openList.put(this.rootCave.getID(),this.rootCave);

		// Get the X and Y axis scalars used for scaling the graph
		int scalarX = visualisation.xScalar();
		int scalarY = visualisation.yScalar();

		while (!openList.isEmpty())
		{
			// Check if Step through button is pressed
			while(!visualisation.isStep())
			{
				try 
				{
					Thread.sleep(100);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
			}
			// Check if Final result button is pressed
			if(!visualisation.isFinalResult())
			{
				visualisation.setStep(false);
				visualisation.repaint();
			}
			// Change to be the lowest score from fScoreMap for all caves from openList
			Cave currentCave = this.lowestScore(openList);

			/*Updating the graph while searching*/
			if(currentCave != rootCave && currentCave != goalCave)
			{
				int windowHeight = 600;
				int xCoord = (int)(currentCave.getX()*scalarX)+20;
				// The Y coordinate is with minus sign as by default the space origin is top-left
				int yCoord = -((int)(currentCave.getY()*scalarY)+30)+windowHeight;
				int diameter = 10;
				
				Oval shape = new Oval(xCoord, yCoord, diameter, Color.YELLOW, currentCave.getID()); 
				visualisation.getShapes().add(shape);
			}

			visualisation.print(currentCaveInfo(currentCave));
			visualisation.print("Open list - caves No: "+ this.openListString(openList.keySet()).toString());
			visualisation.print("\nYou can go in: "+this.neighboursString(currentCave)+"\n");

			if (currentCave == this.goalCave)
			{
				goalReached = true;
				visualisation.print("Success from A*\n");
				visualisation.print("GOAL REACHED\n");
				visualisation.print(getParents(parentPath, currentCave.getID()));
				visualisation.setConsoleColor(Color.GREEN);
				break;
			}
			openList.remove(currentCave.getID());
			closedList.put(currentCave.getID(), currentCave);

			// Iterate though all connections of the current cave
			for (Cave c : this.neighbour(currentCave))
			{
				double temp_cost = currentCave.getGscore() + this.distance(currentCave, c);

				if (openList.get(c.getID()) != null)
				{
					if (openList.get(c.getID()).getGscore() <= temp_cost)
					{
						continue;
					}
				}
				else if (closedList.get(c.getID()) != null)
				{
					// Check if the past value is optimal
					if(closedList.get(c.getID()).getGscore() <= temp_cost)
					{
						continue;
					}
					// Move it back to the open list
					openList.put(c.getID(), c);
					closedList.remove(c.getID());
				}
				else
				{
					c.setHscore(this.distance(c, this.goalCave));					
					openList.put(c.getID(),c);		
				}
				c.setGscore(temp_cost);
				c.setFscore(c.getGscore() + c.getHscore());
				parentPath.put(c.getID(), currentCave);
			}
		}


		if (goalReached)
		{
			// Avoids drawing and modifying the path at the same time
			try 
			{
				Thread.sleep(500);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			// Reconstruct the path
			drawWholePath(parentPath, this.goalCave.getID());
			visualisation.print("\nOverall cost in cm units: "+Double.toString(Math.round(overallDistance*100)/100d));
			visualisation.repaint();	
		}
		else
		{
			// Avoids drawing and modifying the path at the same time
			try 
			{
				Thread.sleep(500);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			visualisation.print("NO SOLUTION EXISTS\n");
			visualisation.setConsoleColor(Color.RED);
			visualisation.repaint();	
		}
	}

	// String representation of the whole path
	private String getParents(TreeMap<Integer, Cave> parentPath, int caveID)
	{
		if (caveID == this.rootCave.getID())
		{
			return "OVERALL PATH: start("+caveID+")";
		}
		Cave parent = parentPath.get(caveID);
		if (caveID == this.goalCave.getID())
		{
			return getParents(parentPath, parent.getID()) + "->end("+caveID+")";
		}
		
		// Calling the function recursively until the starting cave is reached
		return getParents(parentPath, parent.getID()) + "->"+caveID;
	}

	// Adding all tunnels for the path to be drawn
	private void drawWholePath(TreeMap<Integer, Cave> parentPath, int caveID)
	{
		int windowHeight = 600;
		if (caveID == this.rootCave.getID())
		{
			return;
		}
		int scalarX = visualisation.xScalar();
		int scalarY = visualisation.yScalar();

		Cave parent = parentPath.get(caveID);
		Cave currentCave = caveMap.get(caveID);
		
		// Drawing tunnel between the current cave and its' parent
		// The Y coordinate is with minus sign as by default the space origin is top-left
		int x1Coord = (int)(parent.getX()*scalarX)+25;
		int y1Coord = -((int)(parent.getY()*scalarY)+25)+windowHeight;
		int x2Coord = (int)(currentCave.getX()*scalarX)+25;
		int y2Coord = -((int)(currentCave.getY()*scalarY)+25)+windowHeight;
		Line tunnel = new Line(x1Coord, y1Coord, x2Coord, y2Coord,Color.ORANGE, "tunnel");
		
		overallDistance += tunnel.distance(parent.getX(), parent.getY(),currentCave.getX(),currentCave.getY());
		visualisation.getShapes().add(tunnel);
		
		// Calling the function recursively until the starting cave is reached
		drawWholePath(parentPath, parent.getID());
	}
}
