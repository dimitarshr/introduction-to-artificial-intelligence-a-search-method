/* 
 * Oval.java - Inherits the methods from Drawable.java and implements them
 * Contains the necessary methods to draw an oval (cave)
 * Author: Dimitar Hristov, 40201757
 * Module: Artificial Intelligence (SET08118)
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class Oval implements Drawable{

	private int x;
	private int y;
	private int diameter;
	private Color color;
	private int ID;
	
	Oval(int x, int y, int diameter, Color color, int ID)
	{
		this.x = x;
		this.y = y;
		this.diameter = diameter;
		this.color = color;
		this.ID = ID;
	}
	
	// Draw the Oval
	public void draw(Graphics g)
	{
		g.fillOval(x, y, diameter, diameter);
		
		// Set the font and color of the string
		Font font = new Font("Ariel", Font.BOLD, 12);
		g.setColor(Color.red);
		g.setFont(font);
		g.drawString(Integer.toString(ID), x-10, y+10);
	}
	
	// Returns the color
	public Color getClolor() 
	{
		return this.color;
	}
}
