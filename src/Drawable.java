/* 
 * Drawable.java - This is an interface that contains all the properties required to create an object to be drawn.
 * It is inherited and implemented by the Oval and Line Classes. 
 * Author: Dimitar Hristov, 40201757
 * Module: Artificial Intelligence (SET08118)
 */
import java.awt.Color;
import java.awt.Graphics;

public interface Drawable 
{
	// These two methods are implemented in the Line and Oval classes
	public void draw(Graphics g);
	public Color getClolor();
}
