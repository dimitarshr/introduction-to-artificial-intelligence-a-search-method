/* 
 * ReadCaverns.java - Read the file and adds all caves and tunnels to the appropriate data structures
 * Author: Dimitar Hristov, 40201757
 * Module: Artificial Intelligence (SET08118)
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ReadCaverns {

	// Set the values to minimum possible values for integers
	private int maxX = Integer.MIN_VALUE;
	private int maxY = Integer.MIN_VALUE;

	// Returns the max X coordinate from all caves
	public int getMaxX()
	{
		return this.maxX;
	}

	// Returns the max Y coordinate from all caves
	public int getMaxY()
	{
		return this.maxY;
	}
	
	// Reads the selected file and returns a 2D matrix with connectivity information
	public boolean[][] ReadFile(HashMap<Integer,Cave> caveMap) throws IOException 
	{

		// Open input.cav
		BufferedReader br = null;
		JFileChooser fileChooser = new JFileChooser();

		// Restrict the *.* filter file.
		fileChooser.setAcceptAllFileFilterUsed(false);
		// Filter only the *.cav files
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Cave files", "cav"));

		// If a file is selected
		if (fileChooser.showOpenDialog(fileChooser) == JFileChooser.APPROVE_OPTION) 
		{	
			// Get the selected file
			File file = fileChooser.getSelectedFile();	
			br = new BufferedReader(new FileReader(file));
		}
		// If a file is not selected show an error message
		else
		{
			JOptionPane.showMessageDialog(null, "PLEASE SELECT 1 FILE!", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}

		//Read the line of comma separated text from the file
		String buffer = br.readLine(); 
		br.close();

		//Convert the data to an array
		String[] data = buffer.split(",");

		//Now extract data from the array
		int noOfCaves = Integer.parseInt(data[0]);

		//Get coordinates
		for (int count = 1; count < ((noOfCaves*2)+1); count=count+2)
		{
			int xCoord = Integer.parseInt(data[count]);
			int yCoord = Integer.parseInt(data[count+1]);

			// Get the max X and Y used for drawing the caves
			if (xCoord > this.maxX)
			{
				this.maxX = xCoord;
			}
			if (yCoord > this.maxY)
			{
				this.maxY = yCoord;
			}
			// Create a cave and store it in the TreeMap with its ID as a key
			Cave tempCave = new Cave(xCoord,yCoord);
			caveMap.put(tempCave.getID(),tempCave);
		}

		//Build connectivity matrix
		boolean[][] connected = new boolean[noOfCaves][];

		for (int row= 0; row < noOfCaves; row++)
		{
			connected[row] = new boolean[noOfCaves];
		}

		//Now read in the data - the starting point in the array is after the coordinates 
		int col = 0;
		int row = 0;

		for (int point = (noOfCaves*2)+1 ; point < data.length; point++){
			//Work through the array
			if (data[point].equals("1"))
			{
				connected[row][col] = true;
			}
			else
			{
				connected[row][col] = false;
			}
			row++;
			if (row == noOfCaves){ 
				row=0;
				col++;
			}
		}

		return connected;
	}

}
