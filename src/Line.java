/* 
 * Line.java - Inherits the methods from Drawable.java and implements them
 * Contains the necessary methods to draw a line (tunnel)
 * Author: Dimitar Hristov, 40201757
 * Module: Artificial Intelligence (SET08118)
 */
import java.awt.Color;
import java.awt.Graphics;

public class Line implements Drawable{
	private int x1;
	private int y1;
	private int x2;
	private int y2;
	private Color color;
	private String lineType;
	private int lineLength;
	
	Line (int x1,int y1, int x2, int y2, Color color, String lineType)
	{
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.color = color;
		this.lineType = lineType;
	}
	
	Line (int x1,int y1, int x2, int y2, Color color, String lineType, int lineLength)
	{
		// Constructor chaining
		this(x1,y1,x2,y2,color,lineType);
		this.lineLength = lineLength;
	}
	
	// Returns the length of the line
	public double distance(double x1, double y1, double x2, double y2)
	{
		double distance = Math.sqrt(Math.pow((x2-x1), 2) + Math.pow((y2-y1), 2));
		return distance;
	}	
	
	// Draws the line with an arrow head
	public void draw(Graphics g)
	{	
		// Drawing the arrow head
		int dx = x2 - x1, dy = y2 - y1;
        double D = Math.sqrt(dx*dx + dy*dy);
        double xm = D - 20, xn = xm, ym = 4, yn = -4, x;
        double sin = dy/D, cos = dx/D;

        x = xm*cos - ym*sin + x1;
        ym = xm*sin + ym*cos + y1;
        xm = x;

        x = xn*cos - yn*sin + x1;
        yn = xn*sin + yn*cos + y1;
        xn = x;

        int[] xpoints = {x2, (int) xm, (int) xn};
        int[] ypoints = {y2, (int) ym, (int) yn};

        g.fillPolygon(xpoints, ypoints, 3);

        // Drawing the line
		g.drawLine(x1, y1, x2, y2);
		
		// Shows the X and Y axis lengths
		if (this.lineType == "xAxis" || this.lineType == "yAxis" )
		{
			g.drawString(Integer.toString(lineLength), x2, y2);
			g.drawString(Integer.toString(0), x1-10, y1+10);
		}
	}
	
	// Gets the color of the line
	public Color getClolor() 
	{
		return this.color;
	}
}
