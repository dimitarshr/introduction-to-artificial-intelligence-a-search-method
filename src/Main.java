/* 
 * Main.java - Contains the main method of the application, including reading file, drawing and searching. 
 * Author: Dimitar Hristov, 40201757
 * Module: Artificial Intelligence (SET08118)
 */
import java.awt.Color;
import java.io.IOException;
import java.util.HashMap;
import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) 
	{
		// Width and Height of the window
		int windowHeight = 600;
		int windowWidth = 600;
		
		/*READING*/
		//Hashmap containing all caves
		HashMap<Integer,Cave> caveMap = new HashMap<Integer, Cave>();

		//Declare the array showing caves connectivity
		boolean[][] connected = null;

		// Create file pointer and reads all cave objects
		ReadCaverns filePointer = new ReadCaverns();
		try 
		{
			connected = filePointer.ReadFile(caveMap);
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
		}
		
		
		/*GRAPH*/
		Graph visualisation = new Graph(filePointer.getMaxX(), filePointer.getMaxY());
		// X and Y axis
		// The Y coordinates are with minus signs as the space origin is top-left and it has to be moved bottom-left 
		// The length of the x axis is 420
		Line xAxis = new Line(20,-20+windowHeight,440,-20+windowHeight,Color.BLACK, "xAxis", filePointer.getMaxX());
		// The length of the y axis is 420
		Line yAxis = new Line(20,-20+windowHeight,20,-440+windowHeight,Color.BLACK, "yAxis", filePointer.getMaxY());
		visualisation.getShapes().add(xAxis);
		visualisation.getShapes().add(yAxis);

		/*WINDOW*/
		visualisation.setTitle("A* search method");
		visualisation.setSize(windowWidth, windowHeight);
		visualisation.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		visualisation.setVisible(true);
		visualisation.setResizable(false);

		/*DRAWING*/
		visualisation.drawTunnels(caveMap, connected, filePointer.getMaxX(), filePointer.getMaxY());
		visualisation.drawCaves(caveMap, filePointer.getMaxX(), filePointer.getMaxY());

		/*SEARCHING*/
		// Set the initial state and the final goal
		Cave rootCave = caveMap.get(1);
		Cave goalCave = caveMap.get(caveMap.size());

		// Initialize the search object
		FindPath searching = new FindPath(rootCave, goalCave, caveMap, connected, visualisation);

		// Perform A* search
		searching.aStarSearch();
	}

}
