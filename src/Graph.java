/* 
 * Graph.java - Extends the JFrame class and implements the GUI part of the application
 * Author: Dimitar Hristov, 40201757
 * Module: Artificial Intelligence (SET08118)
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

public class Graph extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private ArrayList<Drawable> shapes = new ArrayList<Drawable>();
	private JButton btnStep;
	private JButton btnFinalResult;
	private boolean step = false;
	private boolean finalResult = false;
	private JTextArea  field;
	private JScrollPane scrollConsole;
	private int maxX;
	private int maxY;
	

	public Graph(int maxX, int maxY) 
	{
		// Max X and Y coordinates from caves, used to scale the graph properly
		this.maxX = maxX;
		this.maxY = maxY;

		JPanel pnlHolder = new JPanel();
		this.btnStep = new JButton("Step through");
		this.btnFinalResult = new JButton("Final Result");
		this.field = new JTextArea ();
		this.scrollConsole = new JScrollPane(this.field);
		this.scrollConsole.setPreferredSize(new Dimension(460, 100));
		
		// Keeps the scroll window always at the bottom
		DefaultCaret caret = (DefaultCaret)this.field.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
				
		this.field.setEditable(false);

		pnlHolder.add(this.btnStep);
		pnlHolder.add(this.btnFinalResult);
		pnlHolder.add(this.scrollConsole);
		this.add(pnlHolder);

		// Button to step though the searching process
		this.btnStep.addActionListener(new ActionListener() 
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				setStep(true);
			}
		});
		// Button to show the final result
		this.btnFinalResult.addActionListener(new ActionListener() 
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				setFinalResult(true);
				setStep(true);
			}
		});
		
		// Draw the scene
		this.repaint();
	}

	// Print to the GUI console
	public void print(String output)
	{
		this.field.append(output);
	}
	
	// Get a scalar based on the maximum X coordinate and the x Axis which is 400
	public int xScalar()
	{
		return 400/this.maxX;
	}
	
	// Get a scalar based on the maximum Y coordinate and the y Axis which is 400
	public int yScalar()
	{
		return 400/this.maxY;
	}
	
	// Returns a list a cave that are connected with the tempCave
	private ArrayList<Cave> neighbours(Cave tempCave, boolean[][] connected, HashMap<Integer,Cave> caveMap){
		int from = tempCave.getID() - 1;
		ArrayList <Cave> neightbours = new ArrayList<Cave>();

		for (int index = 0; index < connected.length; index++)
		{
			if (connected[from][index]==true)
			{
				neightbours.add(caveMap.get(index+1));
			}
		}
		return neightbours;
	}
	
	// Draws all tunnels between the caves
	public void drawTunnels(HashMap<Integer,Cave> caveMap, boolean[][] connected, int maxX, int maxY)
	{
		int windowHeight = 600;

		for (Cave c : caveMap.values())
		{
			ArrayList<Cave> neighbours = this.neighbours(c, connected, caveMap);
			for (Cave n : neighbours)
			{
				// The Y coordinates are with minus signs as the space origin is top-left and it has to be moved bottom-left 
				int x1Coord = (int)(c.getX()*this.xScalar())+25;
				int y1Coord = -((int)(c.getY()*this.yScalar())+25)+windowHeight;
				int x2Coord = (int)(n.getX()*this.xScalar())+25;
				int y2Coord = -((int)(n.getY()*this.yScalar())+25)+windowHeight;
				
				Line tunnel = new Line(x1Coord, y1Coord, x2Coord, y2Coord, Color.DARK_GRAY,"tunnel");
				this.shapes.add(tunnel);
			}
		}
		this.repaint();
	}
	
	// Draws all caves
	public void drawCaves(HashMap<Integer,Cave> caveMap, int maxX, int maxY)
	{
		int windowHeight = 600;

		for (Cave c : caveMap.values())
		{	
			Oval shape = null;
			int xCoord = (int)(c.getX()*this.xScalar())+20;
			// The Y coordinate is with minus sign as by default the space origin is top-left
			int yCoord = -((int)(c.getY()*this.yScalar())+30)+windowHeight;
			int diameter = 10;
			
			// The start is green
			if (c.getID() == 1)
			{	
				shape = new Oval(xCoord, yCoord, diameter, Color.GREEN,1); 
			}
			// The goal is red
			else if (c.getID()==caveMap.size())
			{
				shape = new Oval(xCoord, yCoord, diameter, Color.RED, c.getID()); 
			}
			// All the rest caves are blue 
			else
			{
				shape = new Oval(xCoord, yCoord, diameter, Color.BLUE, c.getID());
			}
			this.shapes.add(shape);
		}
		this.repaint();
	}

	// Overriding the paint method
	@Override
	public void paint(Graphics g) 
	{
		super.paint(g);
		draw(g);
	}

	// Draw all elements in the shapes list
	public void draw(Graphics g)
	{ 	
		Graphics2D g2 = (Graphics2D) g;

		for(Drawable s : shapes)
		{
			g2.setColor(s.getClolor());
			s.draw(g2);
		}
	}
	
	// Returns a list of all drawable objects
	public ArrayList<Drawable> getShapes() 
	{
		return this.shapes;
	}
	
	// Used to step through the searching method
	public boolean isStep() 
	{
		return this.step;
	}

	// Used to step through the searching method
	public void setStep(boolean step) 
	{
		this.step = step;
	}

	// Used to go straight to the final result of the searching method
	public boolean isFinalResult() 
	{
		return this.finalResult;
	}
	
	// Used to go straight to the final result of the searching method
	public void setFinalResult(boolean finalResult) 
	{
		this.finalResult = finalResult;
	}
	
	// Set the GUI console color
	public void setConsoleColor(Color color)
	{
		this.field.setBackground(color);
	}
}
